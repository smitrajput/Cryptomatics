pragma solidity >=0.4.22 <0.6.0;
contract Test2 {

string public name;
uint public age=10;
uint public number;

 event SetName(address indexed from, string name);


    constructor (string memory _name) public {
        name = _name;
        age = 20;
        number = 10;
    }
    
    function getName() public view returns (string memory )
    {
        return name;
    }

    function setName(string memory _name) public
    {
        name = _name;
        emit SetName(msg.sender, _name);
    }
    
    function getAge() public view returns (uint  )
    {
        return age;
    }

    function setAge(uint _age) public
    {
        age = _age;
    }
}

