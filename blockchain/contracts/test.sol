pragma solidity >=0.4.22 <0.6.0;
contract Test {

string public name;
uint public age=10;

    constructor (string memory _name) public {
        name = _name;
    }
    
    function getName() public view returns (string memory )
    {
        return name;
    }

    function setName(string memory _name) public
    {
        name = _name;
    }
}

