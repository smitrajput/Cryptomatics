import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { VoteComponent } from './vote/vote.component';
import { VerifyComponent } from './verify/verify.component';
import { KycComponent } from './kyc/kyc.component';
import { AdminComponent } from './admin/admin.component';


const routes: Routes = [
  { path: 'kyc', component: KycComponent },
  { path: 'login', component: LoginComponent },
  { path: 'vote', component: VoteComponent },
  { path: 'resultsverify', component: VerifyComponent },
  { path: 'admin', component: AdminComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
